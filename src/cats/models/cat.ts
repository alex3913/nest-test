import { IsInt, IsString } from 'class-validator';

export class Cat {
  @IsInt()
  id: number;

  @IsString()
  name: string;

  @IsInt()
  age: number;

  @IsString()
  breed: string;

  constructor(cat: Partial<Cat>) {
    Object.assign(this, cat);
  }
}
