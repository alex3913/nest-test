import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { Roles } from '../../decorators/roles.decorator';
import { RolesGuard } from '../../guards/roles.guard';
import { Cat } from '../models/cat';
import { CatsService } from '../services/cats.service';

@Controller('cats')
@UseGuards(RolesGuard)
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Post()
  @Roles('admin')
  async create(@Body() cat: Omit<Cat, 'id'>): Promise<void> {
    return this.catsService.create(cat);
  }

  @Get()
  async findAll(): Promise<Cat[]> {
    return this.catsService.findAll();
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return `This action returns a #${id} cat`;
  }

  @Put(':id')
  @Roles('admin')
  updateById(@Param('id') id: string, @Body() cat: Partial<Cat>) {
    return `This action updates a #${id} with ${cat}`;
  }

  @Delete(':id')
  @Roles('admin')
  deleteById(@Param('id') id: string) {
    return `This action removes a #${id} cat`;
  }
}
