import { Injectable } from '@nestjs/common';
import { Cat } from '../models/cat';

@Injectable()
export class CatsService {
  private readonly cats: Cat[] = [];

  create(cat: Omit<Cat, 'id'>) {
    this.cats.push(new Cat(cat));
  }

  findAll(): Cat[] {
    return this.cats;
  }
}
